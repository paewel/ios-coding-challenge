//
//  CodingChallengeTests.swift
//  CodingChallengeTests
//
//  Created by Brady Miller on 4/7/21.
//

import XCTest
@testable import CodingChallenge

class ShiftsURLProviderTests: XCTestCase {
    func testFirstURL() throws {
        let provider = ShiftsURLProvider()
        let url = provider.url(lastDownloadedShift: nil)

        let formatter =  DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"

        let date = formatter.string(from: Date())

        XCTAssertEqual(url?.absoluteString, "https://staging-app.shiftkey.com/api/v2/available_shifts?address=Dallas,%20TX&type=week&start=\(date)")
    }
}
