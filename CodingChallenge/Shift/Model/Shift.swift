//
//  Shift.swift
//  CodingChallenge
//
//  Created by Pawel on 12/15/21.
//

import Foundation

struct Shift: Decodable, Identifiable {
    var id: Int { shiftId }
    let shiftId: Int
    let startTime: Date
    let endTime: Date
    let normalizedStartDateTime: String
    let normalizedEndDateTime: String
    let timezone: String
    let premiumRate: Bool
    let covid: Bool
    let shiftKind: String
    let withinDistance: Int
    let facilityType: FacilityType
    let skill: Skill
    let localizedSpecialty: LocalizedSpecialty
}

extension Shift {
    struct FacilityType: Decodable {
        let id: Int
        let name: String
        let color: String
    }
}

extension Shift {
    struct Skill: Decodable {
        let id: Int
        let name: String
        let color: String
    }
}

extension Shift {
    struct LocalizedSpecialty: Decodable {
        let id: Int
        let specialtyId: Int
        let stateId: Int
        let name: String
        let abbreviation: String
        let specialty: Specialty
    }
}

extension Shift.LocalizedSpecialty {
    struct Specialty: Decodable {
        let id: Int
        let name: String
        let color: String
        let abbreviation: String
    }
}
