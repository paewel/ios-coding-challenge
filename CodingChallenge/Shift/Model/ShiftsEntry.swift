//
//  ShiftsEntry.swift
//  CodingChallenge
//
//  Created by Pawel on 12/15/21.
//

import Foundation

struct ShiftsEntry: Decodable, Identifiable {
    var id: String { date }
    let date: String
    let shifts: [Shift]
}
