//
//  Shift+Equatable.swift
//  CodingChallenge
//
//  Created by Pawel on 12/15/21.
//

import Foundation

extension Shift: Equatable {
    static func == (lhs: Shift, rhs: Shift) -> Bool {
        lhs.shiftId == rhs.shiftId
    }
}
