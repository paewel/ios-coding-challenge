//
//  ShiftsEntry+lastShift.swift
//  CodingChallenge
//
//  Created by Pawel on 12/15/21.
//

import Foundation

extension Array where Iterator.Element == ShiftsEntry {
    func lastShift() -> Shift? {
        last?.shifts.last
    }
}
