//
//  ShiftsViewModel.swift
//  CodingChallenge
//
//  Created by Pawel on 12/15/21.
//

import Combine
import Foundation
import SwiftUI

class ShiftsViewModel: ObservableObject {

    private let network = Network()

    private var lastShift: Shift?
    @Published var shifts = [ShiftsEntry]() {
        didSet {
            lastShift = shifts.lastShift()
        }
    }
    @Published private var error: String?

    private var disposables = Set<AnyCancellable>()

    func fetchData(currentShift: Shift?) {
        if lastShift == currentShift {
            retrieveShiftData()
            lastShift = nil
        }
    }

    private func retrieveShiftData() {
        network.retrieveShiftData(lastDownloadedShift: lastShift)
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { [weak self] value in
                    guard let self = self else { return }
                    switch value {
                    case .failure(let error):
                        self.error = error.localizedDescription
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] response in
                    guard let self = self else { return }
                    let shifts = response.data.filter {
                        !$0.shifts.isEmpty
                    }
                    self.shifts.append(contentsOf: shifts)
                })
            .store(in: &disposables)
    }
}
