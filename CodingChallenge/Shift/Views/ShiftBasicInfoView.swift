//
//  ShiftBasicInfoView.swift
//  CodingChallenge
//
//  Created by Pawel on 12/15/21.
//

import SwiftUI

struct ShiftBasicInfoView: View {
    var shift: Shift
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("From")
                Text("To")
                Text("Shift kind")
                Text("Facility type")
                Text("Required skill")
            }
            Spacer()
            VStack(alignment: .leading) {
                Text(shift.normalizedStartDateTime)
                Text(shift.normalizedEndDateTime)
                Text(shift.shiftKind)
                Text(shift.facilityType.name)
                Text(shift.skill.name)
            }
        }
    }
}
