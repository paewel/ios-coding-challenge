//
//  ShiftExtendedInfoView.swift
//  CodingChallenge
//
//  Created by Pawel on 12/15/21.
//

import Foundation
import SwiftUI

struct ShiftExtendedInfoView: View {
    var shift: Shift

    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("From")
                Text("To")
                Text("Shift kind")
                Text("Facility type")
                Text("Required skill")
                Text("Time zone")
                Text("Rremium rate")
                Text("Covid")
                Text("Distance")
                Text("Specialty")
            }
            Spacer()
            VStack(alignment: .leading) {
                Text(shift.normalizedStartDateTime)
                Text(shift.normalizedEndDateTime)
                Text(shift.shiftKind)
                Text(shift.facilityType.name)
                Text(shift.skill.name)
                Text(shift.timezone)
                Text("\(shift.premiumRate ? "Yes" : "No")")
                Text("\(shift.covid ? "Yes" : "No")")
                Text("\(shift.withinDistance)")
                Text(shift.localizedSpecialty.name)
            }
        }
        .padding()
    }
}
