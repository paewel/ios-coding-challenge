//
//  ShiftsView.swift
//  CodingChallenge
//
//  Created by Brady Miller on 4/7/21.
//

import SwiftUI
import Combine

struct ShiftsView: View {
    
    @StateObject private var viewModel = ShiftsViewModel()
    
    var body: some View {
        List {
            ShiftListView(shiftsEntries: viewModel.shifts, viewModel: viewModel)
        }
        .task {
            viewModel.fetchData(currentShift: nil)
        }
        
    }
}

struct ShiftsView_Previews: PreviewProvider {
    static var previews: some View {
        ShiftsView()
    }
}
