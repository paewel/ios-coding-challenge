//
//  ShiftListView.swift
//  CodingChallenge
//
//  Created by Pawel on 12/15/21.
//

import SwiftUI

struct ShiftListView: View {
    var shiftsEntries: [ShiftsEntry]
    @State var lastShift: Shift?

    @State private var showingSheet = false

    @StateObject var viewModel: ShiftsViewModel

    var body: some View {
        ForEach(shiftsEntries) { section in
            Section(section.date) {
                ForEach(section.shifts) { shift in
                    ShiftBasicInfoView(shift: shift)
                        .onAppear {
                            viewModel.fetchData(currentShift: shift)
                        }
                        .gesture(TapGesture().onEnded {
                            showingSheet.toggle()
                        })
                        .sheet(isPresented: $showingSheet) {
                            ShiftExtendedInfoView(shift: shift)
                        }
                }
            }
        }
        .task {
            lastShift = shiftsEntries.last?.shifts.last
        }
    }
}
