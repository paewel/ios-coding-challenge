//
//  Network.swift
//  CodingChallenge
//
//  Created by Pawel on 12/13/21.
//

import Foundation
import Combine

class Network {

    private let defaultSession = URLSession(configuration: .default)

    func retrieveShiftData(lastDownloadedShift: Shift?) -> AnyPublisher<ShiftsResponse, Error> {
        let urlProvider = ShiftsURLProvider()
        guard let url = urlProvider.url(lastDownloadedShift: lastDownloadedShift) else {
            let error = ShiftsError.network(description: "Couldn't create URL")
            return Fail(error: error).eraseToAnyPublisher()
        }

        return defaultSession
            .dataTaskPublisher(for: url)
            .tryMap({ element -> Data in
                guard let httpResponse = element.response as? HTTPURLResponse,
                      httpResponse.statusCode == 200 else {
                          throw URLError(.badServerResponse)
                      }
                return element.data
            })
            .decode(type: ShiftsResponse.self, decoder: ShiftsResponse.decoder)
            .eraseToAnyPublisher()
    }
}

