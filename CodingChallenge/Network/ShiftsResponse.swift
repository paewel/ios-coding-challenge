//
//  ShiftsResponse.swift
//  CodingChallenge
//
//  Created by Pawel on 12/15/21.
//

import Foundation

struct ShiftsResponse: Decodable {
    let data: [ShiftsEntry]
    let links: [String]
    let meta: Meta

    struct Meta: Decodable {
        let lat: Double
        let lng: Double
    }
}

extension ShiftsResponse {
    static var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }()
}
