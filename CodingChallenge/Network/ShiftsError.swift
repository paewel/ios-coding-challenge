//
//  ShiftsError.swift
//  CodingChallenge
//
//  Created by Pawel on 12/15/21.
//

import Foundation

enum ShiftsError: Error {
    case parsing(description: String)
    case network(description: String)
}
