//
//  ShiftsURLProvider.swift
//  CodingChallenge
//
//  Created by Pawel on 12/15/21.
//

import Foundation

struct ShiftsURLProvider {
    private static var oneDay: DateComponents = {
        var dayComponent = DateComponents()
        dayComponent.day = 1
        return dayComponent
    }()

    private static var formatter: DateFormatter = {
        let formatter =  DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter
    }()

    private static var commonComponents: URLComponents = {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "staging-app.shiftkey.com"
        components.path += "/api/v2/available_shifts"
        return components
    }()

    private func queryItems(lastDownloadedShift: Shift?) -> [URLQueryItem] {
        [.init(name: "address", value: "Dallas, TX"),
         .init(name: "type", value: "week"),
         .init(name: "start", value: shiftStartDate(lastDownloadedShift: lastDownloadedShift))]
    }

    private func shiftStartDate(lastDownloadedShift: Shift?) -> String {
        var date = Date()
        if let startTime = lastDownloadedShift?.startTime,
           let nextDate = Calendar.current.date(byAdding: Self.oneDay, to: startTime) {
            date = nextDate
        }
        return Self.formatter.string(from: date)
    }

    func url(lastDownloadedShift: Shift?) -> URL? {
        var components = Self.commonComponents
        components.queryItems = queryItems(lastDownloadedShift: lastDownloadedShift)
        return components.url
    }
}
