//
//  CodingChallengeApp.swift
//  CodingChallenge
//
//  Created by Brady Miller on 4/7/21.
//

import SwiftUI

@main
struct CodingChallengeApp: App {
    var body: some Scene {
        WindowGroup {
            ShiftsView()
        }
    }
}

/* Next to do:
 - Add error handling, specifically inform the user about possible errors and a way to resolve it.
 - Hire a UI designer.
 - Handle the case where for a given week there are no shifts, but there might be in the following week. Currently this situation will halt the infitite scroll.
 - Add unit tests
*/
